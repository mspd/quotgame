module QuotGame.deb {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.media;
    requires javafx.fxml;
    requires java.management;
    requires com.google.gson;
    requires jdk.jfr;

    exports javaFX;
    exports resources;
    exports program;
    opens resources;
    opens program;
}