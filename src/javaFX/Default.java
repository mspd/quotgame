package javaFX;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.Objects;

public class Default extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        HBox root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("Default.fxml")));
        Scene scene = new Scene(root, 400,400);
        stage.setTitle("QuotGame");
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String[] args) {
        launch();
    }
}
