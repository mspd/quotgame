package resources;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import program.Game;
import program.Main;
import program.Scanner;

import java.util.ArrayList;
import java.util.Optional;

public class DefaultController {
    @FXML
    private ListView<String> processListView;
    @FXML
    private ListView<String> gameListView;
    @FXML
    private Button add;
    @FXML
    private Button start;
    @FXML
    private Button stop;
    @FXML
    private Button update;
    @FXML
    private Button clear;
    @FXML
    private Button export;

    public void initialize() {
        stop.setDisable(true);
        ArrayList<String> listProcess = Scanner.viewProcessList();
        for(String s : listProcess){
            processListView.getItems().add(s);
        }
        update(null);
        listProcess = Main.getGames();
        for(String s : listProcess){
            gameListView.getItems().add(s);
        }
        update(null);
    }
    public void addGame(ActionEvent actionEvent) {
        String selectedItem = processListView.getSelectionModel().getSelectedItem();

        String nom="error";
        TextInputDialog inputDialog = new TextInputDialog("");
        inputDialog.setTitle("Nom du jeu");
        inputDialog.setHeaderText(null);
        inputDialog.setContentText("Nom du jeu :");
        Optional<String> textIn = inputDialog.showAndWait();
        if (textIn.isPresent() && !textIn.get().isEmpty()){
            nom = textIn.get();
        }

        Main.addGame(new Game(selectedItem,nom));
    }
    public void update(ActionEvent actionEvent){
        gameListView.getItems().clear();
        ArrayList<String> listProcess = Main.getGames();
        for(String s : listProcess){
            gameListView.getItems().add(s);
        }
    }
    public void start(ActionEvent actionEvent){
        Main.Start();
        export.setDisable(true);
        start.setDisable(true);
        stop.setDisable(false);
    }
    public void clear(ActionEvent actionEvent){
        Main.clear();
    }
    public void stop(ActionEvent actionEvent){
        Main.End();
        export.setDisable(false);
        stop.setDisable(true);
    }
    public void export(ActionEvent actionEvent){
        Main.Export();
    }
}
