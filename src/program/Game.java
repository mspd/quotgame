package program;
import com.google.gson.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Game {
    private final String id;
    private final String displayName;
    private long startTime;
    private long runTime;

    ////////////////////////// getters setters //////////////////////////
    public String id() {return id;}

    public String displayName() {return displayName;}


    public long startTime() {return startTime;}

    public void setStartTime(long startTime) {this.startTime = startTime;}

    public long runTime() {return runTime;}

    ////////////////////////// constructor //////////////////////////

    public Game(String id, String displayName) {
        this.id = id;
        this.displayName = displayName;
        this.startTime = 0;
        this.runTime = 0;
    }

    ////////////////////////// methods //////////////////////////
    public Boolean equals(Game o){
        return this.id.equals(o.id());
    }
    public String serialized(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
    public void updateGameTime(){
        this.runTime += System.currentTimeMillis()-this.startTime;
        this.setStartTime(System.currentTimeMillis());
    }
    public static void serializedGameList(ArrayList<Game> games, String filename) {
        try (FileWriter writer = new FileWriter(filename)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(games);
            writer.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void addToActive() {
        boolean done = false;
        Iterator<Game> iterator = Main.inactiveGames.iterator();
        while (iterator.hasNext()) {
            Game game = iterator.next();
            if (game.equals(this)) {
                game.setStartTime(System.currentTimeMillis());
                Main.activeGames.add(game);
                iterator.remove();
                done = true;
            }
        }
        if (!done) {
            this.setStartTime(System.currentTimeMillis());
            Main.activeGames.add(this);
        }
    }
    public void clearGameTime(){
        this.startTime = 0;this.runTime = 0;
    }
}
