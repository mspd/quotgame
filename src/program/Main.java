package program;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    static ArrayList<Game> gameList = Loader.load();
    static ArrayList<Game> activeGames = new ArrayList<>();
    static ArrayList<Game> inactiveGames = new ArrayList<>();
    static Timer timer = new Timer();
    public static void main(String[] args) {


    }
    public static class UpdateTask extends TimerTask {
        public void run(){
            ArrayList<String> scan = Scanner.scan(gameList,activeGames);
            Iterator<Game> iterator = activeGames.iterator();
            while (iterator.hasNext()) {
                Game game = iterator.next();
                game.updateGameTime();
                if (!scan.contains(game.id())) {
                    iterator.remove();
                    inactiveGames.add(game);
                }
            }
        }
    }
    public static void clear(){
        for(Game game:gameList){
            game.clearGameTime();
        }
        Game.serializedGameList(gameList,"C:\\Users\\lolo\\Desktop\\Quot\\QuotGame-deb\\src\\resources\\gameList.json");
    }
    public static void Start(){
        timer.schedule(new UpdateTask(),0,5000);
    }
    public static ArrayList<String> getGames(){
        ArrayList<String> games = new ArrayList<>();
        for(Game game:activeGames){
            DecimalFormat df = new DecimalFormat("#.##");
            String formattedTime = df.format((double) game.runTime() / 3.6e6);
            games.add(game.displayName() + ": " + formattedTime + "h");
        }
        return games;
    }
    public static void End(){
        for(Game game:inactiveGames){
            activeGames.add(game);
        }
        Game.serializedGameList(activeGames,"C:\\Users\\lolo\\Desktop\\Quot\\QuotGame-deb\\src\\resources\\gameData.json");
        timer.cancel();
        Game.serializedGameList(gameList,"C:\\Users\\lolo\\Desktop\\Quot\\QuotGame-deb\\src\\resources\\gameList.json");
    }
    public static void addGame(Game g){
        if (g == null) {
            throw new IllegalArgumentException("Game is null");
        }
        gameList.add(g);
    }
    public static void Export(){
        try (FileWriter writer = new FileWriter("C:\\Users\\lolo\\Desktop\\Quot\\QuotGame-deb\\src\\resources\\Discord.json")) {
            writer.write(dataToDiscord());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String dataToDiscord(){
        // Obtenez la date actuelle
        Date date = new Date();

        // Créez un objet SimpleDateFormat avec le format souhaité
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.FRENCH);

        // Formatez la date en utilisant le format spécifié
        String formattedDate = dateFormat.format(date);
        return String.format("{\n" +
                "  \"content\": null,\n" +
                "  \"embeds\": [\n" +
                "    {\n" +
                "      \"title\": \""+formattedDate+"\",\n" +
                "      \"description\": \"**Jeux joué :**"+gameToString().toString()+"\",\n"+
                "      \"color\": null\n" +
                "    }\n" +
                "  ],\n" +
                "  \"attachments\": []\n" +
                "}");
    }
    public static StringBuilder gameToString(){
        // Création de la chaîne formatée
        StringBuilder formattedString = new StringBuilder();

        // Parcourir la liste des jeux
        for (Game game : activeGames) {
            DecimalFormat df = new DecimalFormat("#.##");
            String formattedTime = df.format((double) game.runTime() / 3.6e6);
            // Ajouter le nom du jeu et son temps de jeu à la chaîne formatée
            formattedString.append("\\n-").append(game.displayName()).append(" : ").append(formattedTime).append("h");
        }
        return formattedString;
    }
}