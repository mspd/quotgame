package program;
import com.google.gson.*;
import java.io.FileReader;
import java.util.ArrayList;

public class Loader {
    public static ArrayList<Game> load() {
        ArrayList<Game> games = new ArrayList<>();

        try (FileReader reader = new FileReader("C:\\Users\\lolo\\Desktop\\Quot\\QuotGame-deb\\src\\resources\\gameList.json")) {
            // Créer un parseur JSON
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(reader);
            JsonArray jsonArray = jsonElement.getAsJsonArray();

            // Créer un objet Gson
            Gson gson = new GsonBuilder().create();

            // Parcourir le tableau JSON et désérialiser chaque élément en objet Game
            for (JsonElement element : jsonArray) {
                Game game = gson.fromJson(element, Game.class);
                games.add(game);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return games;
    }

}

