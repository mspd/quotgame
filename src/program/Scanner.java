package program;
import jdk.jfr.Description;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Scanner {
    @Description("Actualise la list des processus ouverts, et ajoute les processus \"trackés\" à notre liste de jeux")
    public static ArrayList<String> scan(ArrayList<Game> wantedGameList,ArrayList<Game> games){
        ArrayList<String> processList=new ArrayList<>();
        try {
            String line;
            Process p = Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                String proce = line.split(" ")[0];
                processList.add(proce);
                for (Game game : wantedGameList) {
                    if (game.id().equals(proce) && !games.contains(game)) {
                        game.addToActive();
                    }
                }
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
        return processList;
    }

    public static ArrayList<String> viewProcessList(){
        ArrayList<String> processList = new ArrayList<>();
        try {
            String line;
            Process p = Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                String proc = line.split(" ")[0];
                processList.add(proc);
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
        processList.removeFirst();
        processList.removeFirst();
        processList.removeFirst();
        return processList;
    }
}
